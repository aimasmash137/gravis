PFont f;
String message = "M.A.S.H.";
float theta;
float scale = 1;
float xpan = 250;
float ypan = 250;
boolean zoomin = false;
boolean zoomout = false;
boolean panup = false;
boolean pandown = false;
boolean panleft = false;
boolean panright = false;
float panspeed = 5;
float zoomspeed = 1.05;

void setup() {
  size(500, 500, P2D);
  f = createFont("Arial",20,true);
}

void draw() {
  translate(width/2,height/2);
  scale(scale);
  translate(-xpan,-ypan);
  background(0);
  fill(#03E4FF);
  textFont(f);                  
  translate(width/2,height/2);  
  rotate(theta);                
  textAlign(CENTER);
  text(message,0,0);
  theta += 0.05;
    if (zoomin) {
    scale *= zoomspeed;
  } if (zoomout) {
    scale /= zoomspeed;
  } if (panup) {
    ypan -= panspeed;
  } if (pandown) {
    ypan += panspeed;
  } if (panleft) {
    xpan -= panspeed;
  } if (panright) {
    xpan += panspeed;
  }
}

void keyPressed() {
    if (keyCode == UP) {
    zoomin = true;
    zoomout = false;
  } if (keyCode == DOWN) {
    zoomout = true;
    zoomin = false;
  } if (key == 'w') {
    panup = true;
    pandown = false;
  } if (key == 's') {
    pandown = true;
    panup = false;
  } if (key == 'a') {
    panleft = true;
    panright = false;
  } if (key == 'd') {
    panright = true;
    panleft = false;
  }
}

void keyReleased() {
    if (keyCode == UP) {
    zoomin = false;
  } if (keyCode == DOWN) {
    zoomout = false;
  } if (key == 'w') {
    panup = false;
  } if (key == 's') {
    pandown = false;
  } if (key == 'a') {
    panleft = false;
  } if (key == 'd') {
    panright = false;
  }
}
